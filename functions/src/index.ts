import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin'

admin.initializeApp()
const db = admin.firestore()
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

export const insertUser = functions.auth.user().onCreate(user => {
    db.collection('cart').doc(`${user.uid}`).set({
        totPrice: 0,
        totQuan: 0
    })
    return db.collection('users').doc(`${user.uid}`).set({
        name: user.displayName,
        email: user.email,
        photoUrl: user.photoURL,
        phone: user.phoneNumber,
        type: 'buyer',
        address: null,
        pinCode: null
    })
})

export const cartUpdaterOnNewValue = functions.firestore.document("cart/{uid}/items/{docId}").onCreate((data, cont) => {

    const dataVal = data.data() || {};
    const amount = dataVal["price"];
    const quan = dataVal["quantity"];
    const uid = cont.params.uid;

    return db.runTransaction((t) => {
        const ref = db.doc(`cart/${uid}`);
        return t.get(ref).then((doc) => {
            // @ts-ignore
            const total = doc.data().totPrice + (amount * quan);
            // @ts-ignore
            const finalQuan = doc.data().totQuan + (quan);
            t.update(ref, {
                'totPrice': total,
                'totQuan': finalQuan
            })
        })
    });
});

export const cartUpdaterOnValue = functions.firestore
    .document('cart/{uid}/items/{pid}')
    .onUpdate((change, context) => {
        const uid = context.params.uid
        const newVal = change.after.data() || {}
        const oldVal = change.before.data() || {}
        const price = newVal.price
        const oldQuantity = oldVal.quantity
        const newQuantity = newVal.quantity
        const addToTotalQuan = newQuantity - oldQuantity
        const addToTotalPrice = addToTotalQuan * price

        return db.runTransaction(t => {
            const ref = db.doc(`cart/${uid}`)
            return t.get(ref).then(doc => {
                // @ts-ignore
                const finalQuantity = doc.data().totQuan + addToTotalQuan
                // @ts-ignore
                const finalPrice = doc.data().totPrice + addToTotalPrice
                t.update(ref, {
                    totPrice: finalPrice,
                    totQuan: finalQuantity
                })
            })
        })
    })

export const cartUpdaterOnValueDelete = functions.firestore.document('cart/{uid}/items/{pid}').onDelete((snap, context) => {
    const delData = snap.data() || {}
    const uid = context.params.uid
    const price = delData.price
    const quantity = delData.quantity
    const subFromTotalPrice = price * quantity

    return db.runTransaction(t => {
        const ref = db.doc(`cart/${uid}`)
        return t.get(ref).then(doc => {
            // @ts-ignore
            const finalPrice = doc.data().totPrice - subFromTotalPrice
            // @ts-ignore
            const finalQuantity = doc.data().totQuan - quantity
            t.update(ref, {
                totPrice: finalPrice,
                totQuan: finalQuantity
            })
        })
    })
})