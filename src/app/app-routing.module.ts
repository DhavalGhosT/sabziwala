import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './main/product/product.component';
import { ProductAddComponent } from './seller/product-add/product-add.component';
import { CartComponent } from './main/cart/cart.component';
import { DeliveryComponent } from './del/delivery/delivery.component';
import { LoginComponent } from './login/login.component';
import { NavigationComponent } from './navigation/navigation.component';
import { CheckoutComponent } from './main/checkout/checkout.component';
import { SingleProductComponent } from './main/single-product/single-product.component';
import { ProfileComponent } from './main/profile/profile.component';
import { PaymentComponent } from './main/payment/payment.component';


const routes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  {
    path: 'main',
    component: NavigationComponent,
    children: [
      {
        path: '',
        redirectTo: 'product',
        pathMatch: 'full'
      },
      {
        path: 'product',
        component: ProductComponent
      },
      {
        path: 'delivery',
        component: DeliveryComponent
      },
      {
        path: 'productAdd',
        component: ProductAddComponent
      },
      {
        path: 'cart',
        component: CartComponent,
        
      },

      {
        path: 'checkout',
        component: CheckoutComponent
      },
      {
        path: 'product/single_product',
        component: SingleProductComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      
    ]
  },
  {
    path: 'product/delivery',
    component: DeliveryComponent,
    outlet: 'main',
  },
  {
    path: 'payment',
    component: PaymentComponent,
  },

  
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
