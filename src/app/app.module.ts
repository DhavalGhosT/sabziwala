import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SlideshowModule, IImage } from 'ng-simple-slideshow';
import { SlickModule } from 'ngx-slick';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
// import {CommonModule} from '@angular/common'


import { environment } from '../environments/environment'
import { AngularFireModule } from '@angular/fire'
import { AngularFireAuthModule } from '@angular/fire/auth'
import { AngularFirestoreModule } from '@angular/fire/firestore'


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductComponent } from './main/product/product.component';
import { CartComponent } from './main/cart/cart.component';
import { ProductAddComponent } from './seller/product-add/product-add.component';
import { DeliveryComponent } from './del/delivery/delivery.component';
import { LoginComponent } from './login/login.component';
import { NavigationComponent } from './navigation/navigation.component';
import { CheckoutComponent } from './main/checkout/checkout.component';
import { SingleProductComponent } from './main/single-product/single-product.component';
import { ProfileComponent } from './main/profile/profile.component';
import { PaymentComponent } from './main/payment/payment.component';
// import { CommonModule } from '@angular/common';

// import { NgxImageZoomModule } from 'ngx-image-zoom';
// import { NgxImgZoomModule } from 'ngx-img-zoom';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    CartComponent,
    ProductAddComponent,
    DeliveryComponent,
    LoginComponent,
    NavigationComponent,
    CheckoutComponent,
    SingleProductComponent,
    ProfileComponent,
    PaymentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SlideshowModule,
    SlickModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    ReactiveFormsModule,
    FormsModule,
    // CommonModule,
    // NgxImgZoomModule,
    // NgxImageZoomModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
