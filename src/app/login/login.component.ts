import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  emailId = new FormControl('');
  

  constructor(
    private ls: LoginService,
    private router: Router) { }

  ngOnInit() {
    this.ls.getLoggedInUser().subscribe(user => {
      if (user) {
        console.log(user.displayName)
        this.router.navigate([''])
      }
    })
  }

  login() {
    this.ls.loginGoogle()
  }

  logout() {
    this.ls.logout()
  }
}
