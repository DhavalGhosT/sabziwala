import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Product } from 'src/app/models/product';
import { LoginService } from 'src/app/services/login.service';
import { User } from 'src/app/models/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  currUser: firebase.User
  userDet: User
  cartProducts: Observable<Product[]>
  products: Observable<Product[]>
  cartDetails: CartDetails



  constructor(

    private ps: ProductService,
    private afs: AngularFirestore,
    private ls: LoginService
  ) { }

  total
  purchase

  calculateTotalPrice(prod) {
    // this.purchase=this.purchase+(a*b);
    // return a*b
    console.log(prod)
    // for (let index = 0; index < prod.length; index++) {
    //   console.log("jhhbhbh")

    // }

    // this.purchase=this.purchase+(prod.quantity*prod.price)
    // this.total=this.total+this.purchase

  }

  calculateSingleProductPrice(x, y) {
    return x * y;
  }

  // total=20+this.purchase;

  // count=0;



  increment(a) {
    // console.log(a)
    console.log(this.products)
    a.quantity = a.quantity + 1;
    // console.log(a)
    this.ps.addProductToCart(a)
  }

  decrement(b) {
    // console.log(b)
    var val = b.quantity
    if (!val) return;
    val--;
    b.quantity = val
    // console.log(b)
    this.ps.addProductToCart(b)
  }

  removeFromCart(prod) {
    console.log("sdgfjdsbjcjnahjv")
    this.afs.collection(`cart/${this.currUser.uid}/items`).doc(`${prod.pid}`).delete()

  }

  convertJson(prod) {
    console.log("prinying json:" + JSON.stringify(prod))
  }

  ngOnInit() {
    this.ls.getLoggedInUser().subscribe(user => {
      this.currUser = user
      this.cartProducts = this.afs.collection(`cart/${user.uid}/items`).valueChanges()

      this.afs.doc(`cart/${user.uid}`).valueChanges().subscribe(data => {
        console.log(data)
        this.cartDetails = data
      })



      this.ls.getLoggedInUser().subscribe(user => {
        this.afs.doc(`users/${user.uid}`).valueChanges().subscribe(d => {
          this.currUser = user
          const data: User = d
          if (data.pinCode) {
            this.products = this.afs.collection(
              'products',
              ref => ref.where('pinCodes', '==', data.pinCode)
            ).valueChanges()
          } else {
            this.products = this.afs.collection('products').valueChanges()
          }

        })
      })

      // this.convertJson(this.products)

    })
    // console.log(this.cartProducts)
    // this.calculateTotalPrice(JSON.stringify(this.cartProducts))

  }

}
class CartDetails {
  totPrice?: number
  totQuan?: number
}