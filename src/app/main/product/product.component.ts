import { Component, OnInit } from '@angular/core';
import { SlideshowModule, IImage } from 'ng-simple-slideshow';
import { Observable } from 'rxjs';
import { ProductDetails } from 'src/app/models/product-details';
import { LoginService } from 'src/app/services/login.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from 'src/app/models/user';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})



export class ProductComponent implements OnInit {


  currUser: firebase.User
  products: Observable<Product[]>


  imageurl: (string | IImage)[] = [
    { url: '../../../assets/img45.jpg' },
    { url: '../../../assets/img121.jpg' },
  ];
  height: string = '60px';
  minHeight: string = '600px';
  arrowSize: string = '30px';
  showArrows: boolean = true;
  disableSwiping: boolean = false;
  autoPlay: boolean = true;
  autoPlayInterval: number = 3333;
  stopAutoPlayOnSlide: boolean = true;
  debug: boolean = false;
  backgroundSize: string = 'cover';
  backgroundPosition: string = 'center center';
  backgroundRepeat: string = 'no-repeat';
  showDots: boolean = true;
  dotColor: string = '#000000';
  showCaptions: boolean = true;
  captionColor: string = '#FFF';
  captionBackground: string = 'rgba(0, 0, 0, .35)';
  lazyLoad: boolean = false;
  hideOnNoSlides: boolean = false;
  width: string = '100%';
  fullscreen: boolean = false;
  enableZoom: boolean = false;
  enablePan: boolean = false;

  
  slideConfig = {
    "slidesToShow": 4,
    "slidesToScroll": 2,
    "dots": true,
    "infinite": true,
    "autoplay": false,
    "autoplaySpeed": 1500,
    // "accessibility": true,
    "arrows": true,
    "prevArrow": '<button type="button" data-role="none" class="slick-prev time_pass">Previous</button>',
    "nextArrow": '<button type="button" data-role="none" class="slick-next">Next</button>',
    // "appendArrows": true,
    // "centerMode": true,
    // "centerPadding":1000,
  };

  count=[];
  currentProd: Product
  

  afterChange(e) {
    console.log('afterChange');
  }

  constructor(
    private ls: LoginService,
    private afs: AngularFirestore,
    private ps: ProductService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {

  }

  ngOnInit() {
    this.ls.getLoggedInUser().subscribe(user => {
      this.afs.doc(`users/${user.uid}`).valueChanges().subscribe(d => {
        this.currUser = user
        const data: User = d
        if (data.pinCode) {
          this.products = this.afs.collection(
            'products',
            ref => ref.where('pinCodes', '==', data.pinCode)
          ).valueChanges()
        } else {
          this.products = this.afs.collection('products').valueChanges()
        }

      })
    })
    this.ps.currentProduct.subscribe(prod => this.currentProd = prod)
  }


  passProduct(prod: Product) {
    console.log(prod)
    this.ps.changeProduct(prod)
    this.router.navigate(['single_product'],{relativeTo:this.activatedRoute})
  }

}

