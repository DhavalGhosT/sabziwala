import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  userDetails: User
  currUser: firebase.User

  constructor(
    private ls: LoginService,
    private afs: AngularFirestore
  ) { }

  ngOnInit() {
    this.ls.getLoggedInUser().subscribe(user => {
      this.currUser = user
      this.afs.doc(`users/${user.uid}`).valueChanges().subscribe(data => this.userDetails = data)
      this.afs.doc(`cart/${user.uid}`).valueChanges().subscribe(d => {
        console.log(d)
        if (d) console.log('haga')
        else console.log('true')
      })
    })
  }

  changeProfile(userDet: User) {
    console.log(userDet)
    this.afs.doc(`users/${this.currUser.uid}`).update(userDet)
  }

}
