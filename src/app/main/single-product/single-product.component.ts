import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.css']
})
export class SingleProductComponent implements OnInit {

  count=0;
  currentProd: Product

  // getInitialCount(prod: Product){
  //   this.count=prod.quantity
  // }
  

  increment(){
    this.count++;
  }

  decrement(){
    if(!this.count) return;
    this.count--;
  }

  getdisplay='none'


  addToCart(prod: Product){
    if(this.count==0) {
      // this.getdisplay();

      this.getdisplay='block'
      return};
    prod.quantity=this.count
    this.ps.addProductToCart(prod)
  }

  constructor(private ps: ProductService) { }

  ngAfterViewInit() {
    this.getdisplay = 'none'; 
  }

  ngOnInit() {
    this.ps.currentProduct.subscribe(prod => this.currentProd = prod)
    // this.getdisplay='none'
  }

}
