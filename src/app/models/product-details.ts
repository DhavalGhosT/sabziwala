export class ProductDetails {
  pid?: string
  name?: string
  desc?: string
  image?: string
  quantity?: number
  price?: number
  sid?: string
  pinCodes?: number
}


