export class Product {
  pid?: string
  sid?: string
  name?: string
  desc?: string
  image?: string | ArrayBuffer
  quantity?: number
  pinCodes?: number
  price?: number
}

export class ProductSeller {
  sid?: string
  quantity?: number
  price?: number
  rating?: number
}
