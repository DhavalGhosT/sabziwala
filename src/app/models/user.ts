export class User {
  name?: string
  email?: string
  photoUrl?: string
  address?: string
  type?: string
  phone?: number
  pinCode?: number
}
