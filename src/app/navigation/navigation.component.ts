import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router'
import { User } from '../models/user';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  mobNav: boolean = false;
  currUser: firebase.User
  user: User

  constructor(
    private loginService: LoginService,
    private router: Router,
    private afs: AngularFirestore
  ) { }

  ngOnInit() {
    this.loginService.getLoggedInUser().subscribe(user => {
      if (user) {
        this.currUser = user
        console.log(this.currUser.displayName)
        this.afs.doc(`users/${user.uid}`).valueChanges().subscribe(d => {
          if (d.type == 'seller') {
            this.router.navigate(['main/productAdd'])
          }
          this.user = d
        })
      } else {
        console.log('User not logged in')
      }
    })
  }

  goToMainPage() {
    this.router.navigate(['main'])
  }

  toggleMobileNav() {
    this.mobNav = !this.mobNav;
  }

  logout() {
    this.loginService.logout()
  }
}
