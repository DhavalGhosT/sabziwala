import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore'
import { LoginService } from 'src/app/services/login.service';
import { Product, ProductSeller } from 'src/app/models/product';
import { ProductDetails } from 'src/app/models/product-details';
import { ProductService } from 'src/app/services/product.service';
import { Observable, VirtualTimeScheduler } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {
  name: string = ''
  desc: string = ''
  image: string | ArrayBuffer = ''
  price: string = ''
  quantity: string = ''
  soldProducts: Observable<Product[]>
  selectedImage: File = null
  prevInfoEvent: any
  moreInfoButtonText = 'More Info'
  lessInfoButtonText = 'Less Info'
  buffer = this.moreInfoButtonText
  product: Product
  infoFlag = false
  editFlag = false
  submitFlag = false
  imageUploadFlag = false
  prodImageFlag = false
  constructor(
    private ps: ProductService,
    private afs: AngularFirestore,
    private ls: LoginService,
    private router: Router
  ) {
    this.ls.getLoggedInUser().subscribe(user => {
      if (user) {

        this.afs.collection('users', ref => ref.where('type', '==', 'seller'))
          .valueChanges().subscribe(data => {
            data.forEach(userData => {
              console.log(userData)
            })
          })
      }

    })
  }

  ngOnInit() {
    // this.ps.getProducts().subscribe(d => console.log(d))
    this.ls.getLoggedInUser().subscribe(user => {
      this.soldProducts = this.afs.collection('products',
        ref => ref.where('sid', '==', user.uid)).valueChanges()
    })
  }



  changeProductImage(event, prod: Product) {
    this.prodImageFlag = true
    const reader = new FileReader()
    reader.readAsDataURL(event.target.files[0])
    reader.onload = () => {
      prod.image = reader.result
      this.prodImageFlag = false
    }
  }

  onImageSelected(event) {
    console.log(event)
    this.selectedImage = event.target.files[0]
    console.log(this.selectedImage)
    const reader = new FileReader()
    console.log(reader.readAsDataURL(this.selectedImage))
    reader.onload = () => {
      // console.log(reader.result)
      this.image = reader.result
      this.imageUploadFlag = true
    }
  }

  newProduct() {
    if (this.name == '' || this.price == '' || this.quantity == '' || this.desc == '') {
      this.submitFlag = true
      console.log('mt field')
      return
    } else this.submitFlag = false
    console.log(this.name, this.image, this.quantity, this.price, this.desc)
    this.ps.addNewProduct(this.name, this.desc, this.image, Number(this.quantity), Number(this.price))
    this.name = this.image = this.desc = this.quantity = this.price = ''
    this.imageUploadFlag = false
  }

  moreInfo(event, prod: Product) {


    if (this.product == null) {
      this.infoFlag = true
      this.prevInfoEvent = event
      event.srcElement.innerHTML = this.lessInfoButtonText
      this.product = prod
    } else if (this.product != prod) {
      // console.log('in', this.prevInfoEvent && this.prevInfoEvent != event, this.prevInfoEvent, event)
      this.infoFlag = true
      if (this.prevInfoEvent && !this.editFlag) this.prevInfoEvent.srcElement.innerHTML = this.moreInfoButtonText
      event.srcElement.innerHTML = this.lessInfoButtonText
      this.prevInfoEvent = event
      this.product = prod
    } else {
      if (this.infoFlag) {
        this.infoFlag = !this.infoFlag
        event.srcElement.innerHTML = this.moreInfoButtonText
      } else {
        this.infoFlag = !this.infoFlag
        event.srcElement.innerHTML = this.lessInfoButtonText
        this.prevInfoEvent = event
      }
    }
    this.editFlag = false
    // console.log(event == this.prevInfoEvent, 'info, edit', this.infoFlag, this.editFlag)
  }

  editProduct(event, prod: Product) {
    this.infoFlag = false
    if (this.prevInfoEvent) this.prevInfoEvent.srcElement.innerHTML = this.moreInfoButtonText
    if (this.product == null) {
      this.editFlag = true
      this.product = prod

    } else if (this.product != prod) {
      this.editFlag = true
      this.product = prod
    } else {
      this.editFlag = !this.editFlag
    }
    // console.log(event == this.prevInfoEvent, 'info, edit', this.infoFlag, this.editFlag)
  }

  cancelEdit(prod: Product) {
    this.editFlag = false
  }

  applyEdit(prod: Product) {
    this.editFlag = false
    this.afs.doc(`products/${prod.pid}`).update(prod)
  }

  deleteProduct(prod: Product) {
    this.afs.doc(`products/${prod.pid}`).delete()
  }

}
