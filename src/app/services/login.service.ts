import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth'
import { auth } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private af: AngularFireAuth) { }

  loginGoogle() {
    this.af.auth.signInWithPopup(new auth.GoogleAuthProvider())
  }

  logout() {
    this.af.auth.signOut()
  }

  getLoggedInUser() {
    return this.af.authState
  }
}
