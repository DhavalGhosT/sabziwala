import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore'
import { LoginService } from './login.service';
import { Product } from '../models/product';
import { User } from '../models/user';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ProductService {
  currUser: firebase.UserInfo
  user: User
  products: AngularFirestoreCollection<Product>
  private sourceProduct = new BehaviorSubject(null)
  currentProduct = this.sourceProduct.asObservable()

  constructor(
    private afs: AngularFirestore,
    private loginService: LoginService
  ) {
    this.loginService.getLoggedInUser().subscribe(user => {
      if (user) {
        this.currUser = user
        this.afs.doc(`users/${user.uid}`).valueChanges().subscribe(d => {
          this.user = d
          if (this.user.pinCode)
            this.products = this.afs.collection('products', ref => ref.where('pinCodes', '==', this.user.pinCode))
          else this.products = this.afs.collection('products')
        })
      }
    })
  }

  addProductToCart(product: Product) {
    console.log(product)
    this.afs.collection(`cart/${this.currUser.uid}/items`).doc(`${product.pid}`).set(product)
  }

  changeProduct(product: Product) {
    this.sourceProduct.next(product)
    this.currentProduct.subscribe(d => console.log('d', d))
  }

  addNewProduct(name, desc, image, quantity, price) {
    this.afs.doc(`users/${this.currUser.uid}`)
      .valueChanges()
      .subscribe(d => {
        const data: User = d
        const pinCode = data.pinCode
        console.log(pinCode)
        const pid = this.afs.createId()
        // this.afs.collection(`${pinCode}`).doc(pid).set({
        //   pid: pid,
        //   name: name,
        //   desc: desc,
        //   image: image
        // })
        // this.afs.collection(`${pinCode}/${pid}/sellers`).add({
        //     sid: this.currUser.uid,
        //     quantity: quantity,
        //     price: price,
        //     rating: NaN
        // })
        this.afs.collection('products').doc(pid).set({
          pid: pid,
          name: name,
          desc: desc,
          image: image,
          quantity: quantity,
          price: price,
          sid: this.currUser.uid,
          pinCodes: pinCode
        })
      })
  }

}