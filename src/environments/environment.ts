// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBbUk2b5GBt6CqqGsfz8cbczJv9iwsGwJ0",
    authDomain: "sabziwala-e90f8.firebaseapp.com",
    databaseURL: "https://sabziwala-e90f8.firebaseio.com",
    projectId: "sabziwala-e90f8",
    storageBucket: "",
    messagingSenderId: "55408130625",
    appId: "1:55408130625:web:8b02f875f4cd5b28"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
